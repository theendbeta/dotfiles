# This script contains function used to "fix" things. These are things that may or may not be fixed
# in updates in the future.

####################################
# Ubuntu 17.10
# Lenovo T450s
#
# Some touchpad functionality stops working on occasion (after coming out of sleep mode?).
# - Most noticable is that 2-finger scolling stops working
####################################
fix_touchpad_scrolling() {
  sudo modprobe -r psmouse
  sudo modprobe psmouse
}

####################################
# Fedora / Firewalld
# 
# Allow multipass dhcp
####################################
enable_multipass_dhcp() {
  firewall-cmd --zone=trusted --change-interface=mpqemubr0 --permanent
}


plasma-restart() {
  kquitapp5 plasmashell || killall plasmashell && kstart5 plasmashell
}

makemkv_drives() {
  sudo modprobe sg
}

# vim: set expandtab ts=2 sw=2:
