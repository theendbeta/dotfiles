#!/bin/bash

# shellcheck source=./utils.sh
source "$HOME/.common/utils.sh"

# wrapper function to add ssh host keys
function ssh_key_add {
	for var in "$@"; do
		ssh-keyscan "$var" >> ~/.ssh/known_hosts
	done
}

# update the 'title' tag on mkv and mp4 videos to match the filename
function retitle {
	if [[ -z $1 || -d "$1" ]]; then
		local dir;
		if [[ -d "$1" ]]; then
			dir=$1
		else
			dir=$(pwd)
		fi

		for f in "$dir"/*.mkv; do
			mkvpropedit --edit info --set "title=${f:t:r}" "$f";
		done

		for f in "$dir"/*.mp4; do
			mp4tags -s "${f:t:r}" "$f"
		done
	else
		for f in "$@"; do
			if [[ "$1" =~ \.mp4$ ]]; then
				mp4tags -s "${f:t:r}" "$f"
			elif [[ "$1" =~ \.mkv$ ]]; then
				mkvpropedit --edit info --set "title=${f:t:r}" "$f";
			fi
		done
	fi
}

# edit the common shell configuration files, and source afterwards
conf() {
	if command -v chezmoi 1>/dev/null 2>&1; then
		chezmoi edit "$HOME/.common.sh" --watch --apply \
      && chezmoi apply -v
	else
		vim "$HOME/.common.sh" "$HOME/.common/"* "$HOME/.aliases";
	fi
  # shellcheck disable=SC1091
	source "$HOME/.common.sh";
}

# edit the vim configuration files
vimconf() {
	viminit="$HOME/.config/nvim/init.vim"
	if command -v chezmoi 1>/dev/null 2>&1; then
		chezmoi edit "$viminit" --watch --apply \
      && chezmoi apply -v
	else
		vim "$viminit"
	fi
}

clip () {
	if [[ -n "$1" && -f "$1" ]]; then
		if type "wl-copy" > /dev/null; then
			wl-copy < "$1"
		elif type "xclip" > /dev/null; then
			xclip -sel clip < "$1"
		elif type "pbcopy" > /dev/null; then
			pbcopy < "$1"
		else
			echo "No clipboard util available!";
		fi
	fi
}

relink_recipes () {
	orig_dir=$(pwd)
	cd "$HOME/Documents/Recipes/links" || exit 1
	rm ./*.pdf
	find .. -mindepth 1 -type f -path "*.pdf" -exec ln -s {} . \;
	cd "$orig_dir" || exit 1
}

function cpr() {
	rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 "$@"
}

# bitwarden login/auth with tmux support
bw-auth() {
  local bw_session
	bw_session=$(bw unlock --raw)
  if is_tmux; then
		tmux setenv BW_SESSION "$bw_session"
	fi
	export BW_SESSION="$bw_session"
}

v() {
  local fzf_cmd=(fzf)
  local f_cmd=( ls -R -I .git )
  if is_tmux; then
    fzf_cmd=(fzf-tmux -p)
  fi
	if command -v fd 1>/dev/null 2>&1; then
    f_cmd=(fd)
  fi
  file=$("${f_cmd[@]}" | "${fzf_cmd[@]}")
  [ -n "$file" ] && nvim "$file"
}

# ts [session-name]
# Create new tmux session, optionally with the given session name
function ts() {
  local cmd=(tmux new-session)
  if [ -n "$1" ]; then
    cmd+=(-A -s "$1")
  fi
  "${cmd[@]}"
}

# ta
# Attach to an existing tmux session, or switch to that session if
# within a tmux session already
function ta() {
  local fzf_cmd=(fzf)
  local cmd=(tmux list-sessions -F"#S")
  local attach_cmd=(tmux attach-session)
  if is_tmux; then
    fzf_cmd=(fzf-tmux -p --)
    attach_cmd=(tmux switch-client) 
  fi
  fzf_cmd+=(
    --exit-0
    --select-1
    --height "50%"
    --preview 'tmux capture-pane -pt {}'
    --header "Choose TMUX session"
  )
  session=$("${cmd[@]}" | "${fzf_cmd[@]}")
  attach_cmd+=(-t "$session")
  [ -n "$session" ] && "${attach_cmd[@]}"
}

# Export a variable, also setting it in tmux if available
function exp() {
  export "${1}"="${2}"
  if is_tmux; then
    tmux setenv "${1}" "${2}"
  fi
}

wzconnect() {
  setopt localoptions append_create
  local host="$1"
  if [ -z "$host" ]; then
    echo "Hostname required"
  fi

  wezterm connect "SSHMUX:${host}" \
    >>"/tmp/wezterm.${host}.log" \
    2>>"/tmp/wezterm.${host}.err" \
    &
}

wzssh() {
  setopt localoptions append_create
  local host="$1"
  if [ -z "$host" ]; then
    echo "Hostname required"
  fi

  wezterm connect "SSH:${host}" \
    >>"/tmp/wezterm.${host}.log" \
    2>>"/tmp/wezterm.${host}.err" \
    &
}

# vim: set expandtab ts=2 sw=2:
