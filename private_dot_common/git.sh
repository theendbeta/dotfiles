#!/bin/bash

alias g=git
alias gsc='git switch -c'

alias gp='git fetch --all --prune && git pull'
alias gr='cd $(git rev-parse --show-toplevel)'
alias githome='git rev-parse --show-toplevel'


gs() {
	local branch="$1"
	if [ -z "$branch" ]; then
		branch="$(git branch | fzf | tr -d '[:space:]')"
	fi
	if [ -n "$branch" ]; then
		git switch "$branch"
	fi
}

gsa() {
	local branch="$1"
	if [ -z "$branch" ]; then
		branch="$(git branch --all | fzf | tr -d '[:space:]')"
	fi
	if [ -n "$branch" ]; then
		git switch "$branch"
	fi
}

git-rebase-open() {
	nvim $(git status --column --short --porcelain | cut  -d' ' -f 2)
}
