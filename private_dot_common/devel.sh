#!/bin/bash

# shellcheck source=./utils.sh
source "$HOME/.common/utils.sh"

#################
# git
#################

alias git-ssh-debug="GIT_SSH_COMMAND=\"ssh -vvv\" git"
alias git-trace="GIT_TRACE=1 git"

#################
# terraform / terragrunt
#################

alias tf=terraform
alias tg=terragrunt

##################
# containers
##################

alias butane='podman run --rm --interactive       \
              --security-opt label=disable        \
              --volume ${PWD}:/pwd --workdir /pwd \
              quay.io/coreos/butane:release'

##################
# kubernetes
##################

function _kubeconfigs() {
	echo "$HOME/.kube/config"
	if [ -d "$HOME/.kube/config.d" ]; then
		for f in "$HOME/.kube/config.d"/*; do
			echo "$f"
		done
	fi
}

KUBECONFIG="$(join_by : "$(_kubeconfigs)")"
export KUBECONFIG
export KIND_EXPERIMENTAL_PROVIDER=podman

alias kc=kubectl

alias kallns='kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -nl -n'

function ifip() {
	ip -f inet a show dev "$1" | awk '/inet / {print $2}' | sed 's/\/.*//'
}

function dummy-http-redirect() {
	for iface in "dum0" "dum1" "lo"; do
		ip=$(ifip $iface)
		firewall-cmd --direct --add-rule ipv4 nat OUTPUT 0 --source "$ip" --destination "$ip" -p tcp \
			--dport 80  -j DNAT --to "$ip:8080"
		firewall-cmd --direct --add-rule ipv4 nat OUTPUT 0 --source "$ip" --destination "$ip" -p tcp \
			--dport 443 -j DNAT --to "$ip:8443"
	done
}

function dummy-trace() {
  local dummy_ip
  dummy_ip="$(ifip "$1")"
	firewall-cmd --direct --add-rule ipv4 raw OUTPUT 0 --source 0/0 --destination "$dummy_ip" -p tcp --dport 80 -j TRACE
	firewall-cmd --direct --add-rule ipv4 raw OUTPUT 0 --source 0/0 --destination "$dummy_ip" -p tcp --dport 443 -j TRACE
	firewall-cmd --direct --add-rule ipv4 raw PREROUTING 0 --source 0/0 --destination "$dummy_ip" -p tcp --dport 80 -j TRACE
	firewall-cmd --direct --add-rule ipv4 raw PREROUTING 0 --source 0/0 --destination "$dummy_ip" -p tcp --dport 443 -j TRACE
}

function lo-http-redirect() {
	firewall-cmd --direct --add-rule ipv4 nat OUTPUT 0 --source 127.0.0.1 --destination 127.0.0.1 -p tcp --dport 443 -j REDIRECT --to-ports 8443
	firewall-cmd --direct --add-rule ipv4 nat OUTPUT 0 --source 127.0.0.1 --destination 127.0.0.1 -p tcp --dport 80  -j REDIRECT --to-ports 8080
}

#####################################
# AWS
#####################################

# Start an aws-cli container that can be exec'ed into for cli commands
aws-start-container() {
  local image=public.ecr.aws/aws-cli/aws-cli:latest
  [ "$1" == "-u" ] && podman pull "$image"
  podman run \
    --replace \
    -dt \
    --name aws-cli \
    -v "$HOME/.aws:/root/.aws" \
    -v "$HOME:$HOME" \
    --entrypoint /bin/bash \
    -e AWS_PROFILE \
    "$image"
}

aws-prof() {
  local fzf_args=(--select-1 --height=50% --layout=reverse)
  local fzf_cmd=(fzf "${fzf_args[@]}")
  local f_cmd=( aws configure list-profiles )
  if is_tmux; then
    fzf_cmd=(fzf-tmux -p -- "${fzf_args[@]}")
  fi
  prof=$("${f_cmd[@]}" | "${fzf_cmd[@]}")
  if is_tmux; then
    tmux setenv AWS_PROFILE "$prof"
  fi
  export AWS_PROFILE="$prof"
}

aws-setup() {
  if [ -z "$AWS_PROFILE" ]; then
    aws-prof
  fi

  if [ -z "$AWS_PROFILE" ]; then
    echo "AWS Profile required!"
  fi

  aws sso login --no-browser --use-device-code

  local codeartifact_auth_token
  codeartifact_auth_token=$(
    aws codeartifact \
      get-authorization-token \
      --domain etx \
      --domain-owner 744645366470 \
      --profile etxlib \
      --query authorizationToken \
      --output text
  )

  if is_tmux; then
    tmux setenv CODEARTIFACT_AUTH_TOKEN "$codeartifact_auth_token"
  fi
  export CODEARTIFACT_AUTH_TOKEN="$codeartifact_auth_token"
}


#####################################
# python
#####################################

alias por=poetry run

pyact() {
  local git_home
  git_home="$(git rev-parse --show-toplevel)"
  # shellcheck disable=SC1091
  if [ -n "$git_home" ]; then
    source "${git_home}/venv/bin/activate"
  else
    source venv/bin/activate
  fi
}
