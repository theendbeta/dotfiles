# parameter completion for winep script
_winepCompletion () {
	local cur prev prefixes opts

	COMPREPLY=()
	cur=${COMP_WORDS[COMP_CWORD]}
	prev=${COMP_WORDS[COMP_CWORD-1]}

	
	prefixes=$(ls "$WINEHOME")
	opts='--create --use64bit'

	case "$prev" in
		wine)
			COMPREPLY=($( compgen -f -- "${cur}" ))
			;;
		*)
			case "$cur" in 
				-*)
					COMPREPLY=( $( compgen -W "${opts}" -- "$cur" ) )
					;;
				*)
					COMPREPLY=( $( compgen -W "${prefixes}" -- "$cur" ) )
					;;
			esac
			;;
	esac
}

_wineCompletion () {
	local cur actions

	COMPREPLY=()
	cur=${COMP_WORDS[COMP_CWORD]}

	actions='winecfg wineboot'

	# shellcheck disable=SC2034
	COMREPLY=( $( compgen -o filenames -W "${actions}" -- "$cur" ) )
}

complete -F _winepCompletion winep
complete -F _wineCompletion wine


command -v tofu &>/dev/null && complete -o nospace -C "$(command -v tofu)" tofu
command -v terragrunt &>/dev/null && complete -o nospace -C "$(command -v terragrunt)"  terragrunt
command -v aws_completer &>/dev/null && complete -C "$(command -v aws_completer)" aws
