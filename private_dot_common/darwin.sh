#!/usr/bin/env bash

if [ "xDarwin" != "x$(uname -s)" ]; then
  return
fi

_add_to_path "/opt/homebrew/bin" "pre"

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv &>/dev/null || _add_to_path "$PYENV_ROOT/bin"
eval "$(pyenv init -)"

java_home="/Library/Java/JavaVirtualMachines/zulu-8.jdk/Contents/Home"
if [ -d  "$java_home" ]; then
  export JAVA_HOME="$java_home"
fi

SSH_AUTH_SOCK=$(find /private/tmp -path "*com.apple.launchd.*/Listeners" -print -quit 2>/dev/null)
export SSH_AUTH_SOCK

export OPENBLAS=/opt/homebrew/opt/openblas/lib/
export GRPC_PYTHON_BUILD_SYSTEM_OPENSSL=1
export GRPC_PYTHON_BUILD_SYSTEM_ZLIB=1
export CFLAGS=-I/opt/homebrew/opt/openssl/include
export LDFLAGS=-L/opt/homebrew/opt/openssl/lib

alias dev="cd \$HOME/Development/empiricotx"

ssh-agent-local() {
  if [ -n "$SSH_AGENT_PID" ]; then
    ssh-agent -k
  fi
  eval "$(ssh-agent)"
  if is_tmux; then
		tmux setenv SSH_AUTH_SOCK "$SSH_AUTH_SOCK"
		tmux setenv SSH_AGENT_PID "$SSH_AGENT_PID"
	fi
  ssh-add "$HOME/.ssh/id_ed25519_sk-emp-c"

  cat << EOF >| "$HOME/.common/ssh-env.sh"
export SSH_AUTH_SOCK=$SSH_AUTH_SOCK
export SSH_AGENT_PID=$SSH_AGENT_PID
EOF
}


podman-machine-dagger() {
  local vm="dagger-engine"

  podman machine rm --force --save-ignition "$vm" || true

  podman machine init \
    --cpus=8 \
    --disk-size=200 \
    -m=16384 \
    -v "$HOME:$HOME" \
    --ignition-path "$HOME/Development/podman-machine.ign" \
    --now \
    "$vm"
}
