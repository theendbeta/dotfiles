
function join_by { local IFS="$1"; shift; echo "$*"; }

function tree-md {
	tree=$(tree -tf --noreport -I '*~' --charset ascii $1 |
			sed -e 's:\(- \)\(\(.*/\)\([^/]\+\)\):\1[\4](\2):g')
			# sed -e 's/| \+/  /g' -e 's:\(- \)\(\(.*/\)\([^/]\+\)\):\1[\4](\2):g')

	printf "# Project tree\n\n${tree}"
}

function _add_to_path() {
	if [ -d "$1" ]; then
		case ":${PATH:=$1}:" in
			*:"$1":*) ;;
			*) 
        if [ -n "$2" ] && [ "$2" == "pre" ]; then
          PATH="$1:$PATH"
        else 
          PATH="$PATH:$1"
        fi
        ;;
		esac
	fi
}

function bak() {
	file="$1"
	ext="${file##*.}"
	base="${file%.*}"
	if [[ "$ext" == "bak" ]]; then
		mv "${file}" "${base}"
	else
		mv "${file}" "${file}.bak"
	fi
}

is_tmux() {
  [ -n "$TMUX" ] || [ "$TERM_PROGRAM" = "tmux" ]
}
