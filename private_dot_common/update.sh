#!/usr/bin/bash

rust_update() {
  rustup update
}

cargo_update() {
  if ! cargo --list | grep -q "install-update"; then
    cargo install --locked cargo-update
  fi
  cargo install-update --all
}

go_update() {
  if ! command -v gup &>/dev/null; then
    go install github.com/nao1215/gup@latest
  fi
  gup update
}

ansible-galaxy-collection-upgrade() {
  ansible-galaxy collection list --format json |
    jq -rc '.[] | keys | .[]' |
    xargs ansible-galaxy collection install --upgrade
}
