[user]
  email = astein@vesulabs.us
  name  = Aidan Stein
  ; signingkey = ~/.ssh/id_ed25519_sk-a.pub
  signingkey = ~/.ssh/id_ed25519_sk-c.pub
[core]
  editor = nvim
  fsmonitor = true
  #pager  = delta
[difftool]
  prompt = false
[diff]
  tool       = nvimdiff
  colorMoved = zebra
  external   = difft
  algorithm  = histogram
[difftool "nvimdiff"]
  cmd = "nvim -d \"$LOCAL\" \"$REMOTE\""
[merge]
  conflictstyle = zdiff3
  tool          = nvimdiff
  autostash     = true
[mergetool "nvimdiff"]
  cmd = nvim -d $LOCAL $MERGED $REMOTE $BASE -c 'wincmd w' -c 'wincmd J'
[push]
  default         = current
  autoSetupRemote = true
[cola]
  spellcheck = false
[fetch]
  prune     = true
  prunetags = true
[pull]
  rebase    = true
  autostash = true
[rebase]
  autosquash = true
  autostash  = true
  updateRefs = true
[rerere]
  # Reuse recorded resolution of conflicted merges
  enabled = true
[init]
  defaultBranch = main
[credential]
  helper = cache --timeout=3600
[gpg]
  format = ssh
[commit]
  gpgsign = true
  verbose = true
[help]
  autocorrect = prompt
[alias]
  a  = add .
  au = add -u .
  cf = commit --fixup
  ds = diff --staged
  rs = restore --staged
  w  = worktree
  wb = worktree add -b
  wl = worktree list
  l  = log origin/main..
  lp  = log -p origin/main..
  rmain = rebase --autostash --autosquash origin/main
  patchdiff = diff --no-ext-diff --patch

[include]
  path = ~/.config/gitconfig
[includeIf "gitdir:~/Development/"]
  path = ~/Development/.gitconfig

# vim: set expandtab ft=gitconfig:
