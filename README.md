# Dotfiles

This repository contains a script to automatically install ansible and run a playbook to install
dependencies for the dotfiles (nvim plugins, etc.).

## Prereqs

* pip

* [snapd](https://snapcraft.io/docs/installing-snapd)

* [Flatpak](https://flatpak.org/setup/)


```bash
sudo dnf install snapd snapd-selinux python3-pip git

sudo systemctl enable --now snapd
sudo snapd install chezmoi --classic
sudo snapd install bw

bw config server "$your_bitwarden_url"
```



## Quick Setup

```bash
# With chezmoi installed
chezmoi init --apply --verbose --ssh gitlab.com/theEndBeta

# super one-liner
sh -c "$(curl -fsLS chezmoi.io/get)" -b "$HOME/.local/bin" -- init --apply --verbose --ssh gitlab.com/theEndBeta
```


## Management

This repository is managed using [chezmoi](https://chezmoi.io).


### Setup

Adapted from [chezmoi setup](https://chezmoi.io/user-guide/setup):

```bash
# check out the repository
chezmoi init git@gitlab.com:theEndBeta/dotfiles.git

# check differences
chezmoi diff

# apply changes/files
chezmoi apply
```
