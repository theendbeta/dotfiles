local wezterm = require 'wezterm'
local act = wezterm.action
local mux = wezterm.mux

local config = wezterm.config_builder()
config.keys = {}
config.key_tables = {}

config.term = 'wezterm'

config.font = wezterm.font 'Hack Nerd Font Mono'
config.font_size = 11.0

-- tic -x -o ~/.terminfo <(curl https://raw.githubusercontent.com/wez/wezterm/main/termwiz/data/wezterm.terminfo)
config.term = 'wezterm'

config.color_scheme = "GruvboxDark"

config.show_new_tab_button_in_tab_bar = false
config.tab_bar_at_bottom = true
config.mux_enable_ssh_agent = false

wezterm.GLOBAL.env = wezterm.GLOBAL.env or {}


config.leader = { key = "Tab", mods = "CTRL" }

table.insert(config.keys,
  { key = 'r', mods = 'LEADER', action = act.ActivateKeyTable({ name = 'resize_pane', one_shot = false }), }
)

local splitKeys = {
  { key = config.leader.key, mods = "LEADER|" .. config.leader.mods, action = act.SendKey(config.leader) },
  {
    key = "s",
    mods = "LEADER",
    action = act.SplitVertical { domain = "CurrentPaneDomain" }
  },
  {
    key = "S",
    mods = "LEADER",
    action = act.SplitHorizontal { domain = "CurrentPaneDomain" }
  },
  {
    key = "c",
    mods = "LEADER",
    action = act.SpawnTab("CurrentPaneDomain")
  },
  {
    key = "n",
    mods = "LEADER",
    action = act.ActivateTabRelative(1)
  },
  {
    key = "p",
    mods = "LEADER",
    action = act.ActivateTabRelative(-1)
  },
  {
    key = "<",
    mods = "LEADER|SHIFT",
    action = act.MoveTabRelative(-1)
  },
  {
    key = ">",
    mods = "LEADER|SHIFT",
    action = act.MoveTabRelative(1)
  },
}
for _, key in ipairs(splitKeys) do
  table.insert(config.keys, key)
end

for i = 1, 9 do
  table.insert(config.keys, { key = tostring(i), mods = "LEADER", action = act.ActivateTab(i - 1) })
  table.insert(config.keys, { key = tostring(i), mods = "ALT", action = act.ActivateTab(i - 1) })
end

local _movementKeys = {
  { dir = "Left",  keys = { "LeftArrow", "h", "H" } },
  { dir = "Down",  keys = { "DownArrow", "j", "J" } },
  { dir = "Up",    keys = { "UpArrow", "k", "K" } },
  { dir = "Right", keys = { "RightArrow", "l", "L" } },
}
local resizeKeys = {}
for _, mkeys in ipairs(_movementKeys) do
  for _, key in ipairs(mkeys.keys) do
    for _, mod in ipairs({ '', 'CTRL' }) do
      table.insert(config.keys, {
        key = key,
        mods = "LEADER|" .. mod,
        action = act.ActivatePaneDirection(mkeys.dir),
      })
    end
    table.insert(resizeKeys, {
      key = key,
      action = act.AdjustPaneSize({ mkeys.dir, 1 }),
    })
  end
end
table.insert(resizeKeys,
  -- Cancel the mode by pressing escape
  { key = 'Escape', action = 'PopKeyTable' }
)
config.key_tables['resize_pane'] = resizeKeys


config.mouse_bindings = {
  -- Change the default click behavior so that it only selects
  -- text and doesn't open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Right' } },
    mods = 'NONE',
    action = act.CompleteSelection 'ClipboardAndPrimarySelection',
  },

  -- and make CTRL-Click open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'CTRL',
    action = act.OpenLinkAtMouseCursor,
  }
}

return config
