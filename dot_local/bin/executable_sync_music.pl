#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: sync_music.pl
#
#        USAGE: ./sync_music.pl  
#
#  DESCRIPTION: Wrapper around rsync commands to sync a playlist
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 07/03/2016 08:39:07 PM
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Getopt::Long;
use XML::LibXML;

GetOptions(
	'source|s=s'   => \my $source_dir,
	'target|t=s'   => \my $target_dir,
	'playlist|p=s' => \my $playlist_file,
	'dry-run|n'  => \my $dry_run,
) or die ("Error in command line arguments\n");


sub getLines {
	my ($playlist_file) = @_;
	my @lines;
	if ($playlist_file =~ /\.xspf$/) {

		my $dom = XML::LibXML->load_xml(location => $playlist_file);
		my $xc = XML::LibXML::XPathContext->new( $dom );
		$xc->registerNs("ns", "http://xspf.org/ns/0/");

		@lines = $xc->findnodes('//ns:location');
		@lines = map {"+ \"".( $_->to_literal =~ s{\Q$source_dir\E}{}r )."\"\n"} @lines;
		push @lines, "";
	}
	else {
		open(IN, '< :encoding(UTF-8)', $playlist_file) or die "Could not open $playlist_file for read";
		@lines=<IN>;
		close(IN);
		@lines = map { $_ =~ s/^\xEF\xBB\xBF//gr} @lines;
		@lines = map { $_ =~ s/\r//gr} @lines;
	}

	return \@lines;
}

open(OUT, "> :encoding(UTF-8)", $playlist_file."_new") or die "Could not open $playlist_file for write";

my %dirs = ();

foreach my $line (@{getLines($playlist_file)}) {
	my @structure = split /\//, $line;
	pop @structure;
	my $path = "";
	foreach my $piece (@structure) {
		$path .= $piece . "/";
		next if $dirs{$path};
		$dirs{$path} = 1;
		print OUT "$path\"\n";
	}
	print OUT $line;
}

print OUT "- *";

close OUT;

my @rsync_args = (
	"-P", # --partial and --progress (show progress)
	"-m", # prune empty directories (means it won't copy over directories, even though all are included)
	"-r", # recursive
	"-vv", # very verbose (show inclusions and exclusion)
	"--ignore-existing", # don't copy if it already exists
	"--stats", # show overall stats
	"-hhh", # use human readable
	"--delete-excluded",
	"--delete-during",
	"--omit-dir-times",
	"--no-perms",
	"--inplace",
	"--include-from=${playlist_file}_new",
	$source_dir,
	$target_dir,
);

my @rsync_cmd = ('rsync');
push @rsync_cmd, '-n' if $dry_run;

system(@rsync_cmd, @rsync_args);
