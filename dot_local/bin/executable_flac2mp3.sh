#!/bin/bash
shopt -s extglob

###################################################################
#
# Functions
#
###################################################################

function usage() {
	echo USAGE: "./flac2mp3.sh -i <path> -o <path>"
	echo "-i specifies directory with the input files"
	echo "-o specifies the directory to write the files to"
}

function convertFlacToMP3() {
	if [ -z "$1" -o -z "$2" ]; then
		echo "Both source and destination files must be passed."
		exit 1;
	fi

	src="$1"
	dest="$2"

	# Pull tags from the source file - just pull enough for the files to be recognized by beet
	# import
	title=$(metaflac --show-tag=title "$src" | sed 's/title=//i')
	year=$(metaflac --show-tag=year "$src" | sed 's/year=//i')
	artist=$(metaflac --show-tag=artist "$src" | sed 's/artist=//i')
	album=$(metaflac --show-tag=album "$src" | sed 's/album=//i')
	albumartist=$(metaflac --show-tag=albumartist "$src" | sed 's/albumartist=//i')
	totaltracks=$(metaflac --show-tag=totaltracks "$src" | sed 's/totaltracks=//i')
	tracknumber=$(metaflac --show-tag=tracknumber "$src" | sed 's/tracknumber=//i')
	discnumber=$(metaflac --show-tag=discnumber "$src" | sed 's/discnumber=//i')

	if [ -z "$albumartist" ]; then
		albumartist="$artist"
	fi

	if [ -z "$year" ]; then
		year=$(metaflac --show-tag=date "$src" | sed 's/date=//i' | sed 's/-.*//')
	fi

	flac -dc "$src" | lame -V0 --add-id3v2 --pad-id3v2 --ignore-tag-errors \
		--ta "$artist" \
		--tt "$title" \
		--tl "$album" \
		--tn "${tracknumber:-0}" \
		--ty "$year" \
		- "$dest"

	echo "***$year***"
	eyeD3 \
		--title="$title" \
		--release-year="$year" \
		--artist="$artist" \
		--album="$album" \
		--track="$tracknumber" \
		--track-total="$totaltracks" \
		--text-frame="TPE2:$albumartist" \
		--text-frame="TPOS:$discnumber" \
		"$dest"
}


###################################################################
#
# Main
#
###################################################################

while getopts "i:o:h" opt; do
	case $opt in
		i)
			src=$OPTARG
			;;
		o)
			dest=$OPTARG
			;;
		h)
			usage
			exit
			;;
		\?)
			usage
			exit
			;;
	esac
done


if [ ! -e "$src" ]; then
	echo "Source file does not exist!";
	exit 1;
fi

fname=$(basename "$src")
ext="${fname##*.}"
if [ "$ext" -ne "flac" ]; then
	ffmpeg -i "$src" -y -vn -aq 2 "$dest"
else
	convertFlacToMP3 "$src" "$dest"
fi
