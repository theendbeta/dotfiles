#!/usr/bin/python3

from bs4 import BeautifulSoup
import pycurl
import json
import os
import sys
import re
import requests
import mutagen
import mutagen.id3
import mutagen.easyid3
import datetime as dt


def progress(
    total_to_download,
    total_downloaded,
    total_to_upload,
    total_uploaded
):
    '''
    pretty print progress and percentage completed
    '''
    if total_to_download:
        total_mb = round(total_to_download/(1024.0 ** 2), ndigits=2)
        downloaded_mb = round(total_downloaded/(1024.0 ** 2), ndigits=2)
        percent_completed = float(total_downloaded) / total_to_download
        rate = round(percent_completed * 100, ndigits=2)
        sys.stdout.write("\033[K")
        sys.stdout.write("\r{0:.2f}/{1:.2f}MB ({2:.2f}%) downloaded"
                         .format(downloaded_mb, total_mb, rate))
        sys.stdout.flush()


page_url = sys.argv[1]

page = requests.get(page_url)

# only_audio_embed = SoupStrainer(class_="audio-tool-embed")

soup = BeautifulSoup(page.content, 'lxml')
embed_button = soup\
    .find(class_="audio-tool-embed")\
    .find('button', recursive=False)
embeded_player_url = embed_button['data-embed-url']

# print(embed_button)

if not embeded_player_url.startswith('http'):
    embeded_player_url = 'http://' + embeded_player_url

embeded_player_page = requests.get(embeded_player_url)

player_soup = BeautifulSoup(embeded_player_page.content, 'lxml')
# print(embeded_player_url)

# print(player_soup)
api_script = player_soup.find('script', string=re.compile('"audio":'))

api_json = re.findall('^var .*?=\s*(.*?);$', api_script.string)
data = json.loads(api_json[0])
audio_data = data['list']['story'][0]['audio']

mp3_formats = None
for opt in audio_data:
    if opt['format'] and 'mp3' in opt['format']:
        mp3_formats = opt['format']['mp3']

if mp3_formats is None:
    print('No mp3 format found!')
    print(audio_data)
    exit(1)

# Get title by removing the series name
full_title = data['list']['story'][0]['title']['$text']
title = re.sub('The Thistle & Shamrock: ', '', full_title)

# Get episode date
date_str = data['list']['story'][0]['storyDate']['$text']
date = dt.datetime.strptime(date_str, '%a, %d %b %Y %H:%M:%S %z')

url = next(
    (
        mp3_type['$text']
        for mp3_type
        in mp3_formats
        if mp3_type['type'] == 'mp3'
    ),
    None
)

if (url is None):
    exit()

######################################
# Stream and save to file
######################################
filename = "{0} {1}.mp3".format(date.strftime('%Y-%m-%d'), title)
filepath = os.path.join(
    os.environ['HOME'],
    'Music/Podcasts/The Thistle & Shamrock',
    filename
)

print("Downloading \"{}\" to \"{}\"".format(full_title, filepath))

with open(filepath, 'wb') as f:
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, f)
    c.setopt(pycurl.NOPROGRESS, False)
    c.setopt(pycurl.PROGRESSFUNCTION, progress)
    c.perform()
    c.close()

# Write tag information
if (os.path.exists(filepath)):
    audio = mutagen.id3.ID3()
    audio.add(mutagen.id3.TIT2(
        encoding=3,
        text=re.sub('The Thistle & Shamrock: ', '', title)
    ))
    audio.add(mutagen.id3.TPE2(encoding=3, text='The Thistle & Shamrock'))
    audio.add(mutagen.id3.TALB(encoding=3, text='The Thistle & Shamrock'))
    audio.add(mutagen.id3.TPE1(encoding=3, text='The Thistle & Shamrock'))
    audio.add(mutagen.id3.TCON(encoding=3, text='podcast'))
    audio.add(mutagen.id3.TYER(encoding=3, text="{}".format(date.year)))
    audio.save(filename=filepath)


# mp3_stream = requests.get(url, stream=True)
# filename = title + ".mp3"
#
# filepath = os.path.join(
#     os.environ['HOME'],
#     'Music/Podcasts/The Thistle & Shamrock',
#     title + '.mp3'
# )
# print(filepath)

# with open(filepath, 'wb') as fd:
#     for chunk in mp3_stream.iter_content(1024):
#         fd.write(chunk)
