#!/usr/bin/env perl
# vim: set ft=perl

#===============================================================================
#
#         FILE: winep
#
#        USAGE: ./winep [options] <prefix_name> <commands>
#
#  DESCRIPTION: Wrapper to make calling setting up/using wine prefixes easier.
#
#      OPTIONS:
#               use64bit - set WINEARCH enviroment to 'win64' (defaults to 'win32') when creating a
#                          new winebottle. Ignored (detected automatically) for existing bottles
#               create   - create a new winebottle with the given wineprefix
#               debug    - enable wine debugging
#        NOTES: ---
#       AUTHOR: Aidan Stein
#      VERSION: 0.3
#      CREATED: 07/02/2016 09:18:51 PM
#     REVISION: 4
#===============================================================================

use strict;
use warnings;
use utf8;

use File::Path;
use File::Spec;
use Getopt::Long;

GetOptions(
	"use64bit" => \my $use64bit,
	"create"   => \my $create,
	"debug"    => \my $debug,
) or die("Error in command line options\n");

# This is enough to deep copy, since we are passing around strings
my @args = @ARGV;

# Quit unless we have at least two arguments (the wine prefix name and the command to run)
# or we are creating a new bottle
unless (
	scalar(@args) >= 2
	|| (( scalar(@args) == 1 ) && $create)
) {
	print "\nUsage: winep [options] <prefix_name> <commands>\n"; 
	exit;
}

my $winebottle = shift @args;

# construct the root directory for all wineprefixes
my $winehome = $ENV{WINEHOME} // File::Spec->join($ENV{HOME}, ".wine");

# Create the winehome directory if it doesn't exist
if (!dir_exists($winehome)) {
	File::Path::make_path($winehome);
}

my $prefix = File::Spec->join( $winehome, $winebottle );

if (!dir_exists($prefix)) {
	File::Path::make_path($prefix);
}
else {
	if ($create) {
		print "Winebottle $winebottle aleady exists - not overwriting\n.";
	}

	# Autodetect correct WINEARCH to use, if we aren't creating a new bottle
	$use64bit = 0;
	if (dir_exists(File::Spec->join($prefix, 'drive_c/windows/syswow64'))) {
		$use64bit = 1;
	}
}

local $ENV{WINEPREFIX} = $prefix;
local $ENV{WINEARCH} = $use64bit ? 'win64' : 'win32';
local $ENV{WINEDEBUG} = $debug ? 'warn+all' : $ENV{WINEDEBUG};

# run wineboot immediately if user wants to create new bottle
if ($create) {
	system('wine', 'wineboot')
}

# We now know that the bottle has been created, so run any commands that the user specified
system(@args) if scalar(@args);


# helper function to check for directory existence.
sub dir_exists {
	my $path = shift;
	return ( -e $path && -d $path );
}
