#!/usr/bin/env python3

import subprocess
import enzyme
import os
import sys

source = sys.argv[1]

fname = os.path.basename(source)
if (source.endswith('1080p].mkv')):
    fname = fname.replace('[1080p].mkv', '[720p].mp4')
else:
    fname = fname.replace('.mkv', '[720p].mp4')

dest = os.path.join(
    os.environ['HOME'],
    'Videos/chromecast/',
    os.path.basename(fname)
)

with open(source, 'rb') as f:
    mkv = enzyme.MKV(f)

cmd = [
    'HandBrakeCLI',
    '-i', source,
    '-o', dest,

    #'--format', 'mp4',
    '--format', 'av_mp4',
    '--markers',
    '--optimize', # optimize MP3 files for HTTP streaming
    '--encoder', 'x264',
    '--quality', '22.0',
    '--no-two-pass',
    '--rate', '30',
    '--pfr',  # Won't allow rate to go over --rate

    #'--h264-level', '3.1',
    '--h264-level', '4.0',
    #'--x264-preset', 'medium',
    #'--h264-profile', 'high',
    '--encoder-preset', 'medium',
    '--encoder-profile', 'high',

    #'--audio', '0',
    '--first-audio',
    ##'--aencoder', 'av_aac',
    '--aencoder', 'copy:aac',
    '--audio-copy-mask', 'aac,ac3',
    #'--audio-fallback', 'ac3',
    '--audio-fallback', 'ac3',
    '--ab', '320',
    ##'--aq', '-3.0',
    '--ac', '-1.0',
    '--mixdown', 'dpl2',
    #'--mixdown', 'stereo',
    '--arate', 'auto',
    '--drc', '0.0',
    '--adither', 'auto',

    '-X', '1280',
    '-Y', '720',
    '--loose-anamorphic',
#    '--modulus', '2',

    '--first-subtitle',
    '--subtitle-burned',
]

#cmd = [
#    'HandBrakeCLI',
#    '-i', source,
#    '-o', dest,
#    '--preset-import-file',
#    '/media/greatpigeon/Data04/Videos/convert/workout - anime.json'
#]

#sub_track_num = -1

#print(mkv.subtitle_tracks)
#for (i, sub_track) in enumerate(mkv.subtitle_tracks):
#    if sub_track.language == 'eng' or (sub_track.name is not None and sub_track.name.find('English') != -1):
#        sub_track_num = i+1
#    elif sub_track.language == 'und' and sub_track_num == -1:
#        sub_track_num = i+1
#
#if sub_track_num != -1:
#    cmd.extend([
#        '--subtitle', '{}'.format(sub_track_num),
#        '--subtitle-burn', '1'
#    ])

print(" ".join(cmd))
print('{} --> {}'.format(source, dest))
subprocess.run(cmd)
