local opts = { noremap = true, silent = true }

vim.keymap.set('n', '<C-s>', ':%s/')
vim.keymap.set('v', '<C-s>', ':s/')


-- location list movement
vim.api.nvim_set_keymap("n", "<leader>ln", ":lnex<CR>", opts)
vim.api.nvim_set_keymap("n", "<leader>lp", ":lprev<CR>", opts)

-- Buffer movement
-- vim.api.nvim_set_keymap("n", "<leader>n", ":bn<CR>", opts)
-- vim.api.nvim_set_keymap("n", "<leader>p", ":bp<CR>", opts)

------------------------------
-- FZF
------------------------------

vim.keymap.set('n', '<leader>ft', ':BTags<CR>')
vim.keymap.set('n', '<leader>fb', ':Buffers<CR>')
vim.keymap.set('n', '<leader>fl', ':BLines<CR>')
vim.keymap.set('n', '<leader>fh', ':Helptags<CR>')
vim.keymap.set('n', '<leader>fg', ':GFiles<CR>')
vim.keymap.set('n', '<C-g>', ':GFiles<CR>')
vim.keymap.set('n', '<leader>fc', ':Commands<CR>')
vim.keymap.set('n', '<leader>fo', ':GFiles --others --modified --exclude-standard<CR>')
