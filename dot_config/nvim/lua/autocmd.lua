local util = require('lib.utils')

---@class Augroup
---@field clear    boolean   Should the `augroup` be cleared on creation
---@field autocmds Autocmd[] Table  of  A


---@class Autocmd An `autocmd`
---@field event (string | table) Event(s) for the `autocmd`
---@field opts  table            Table of options to be passed to `nvim_create_autocmd`. `group` option is set automatically


---@param name string The name of the `augroup` to create
---@param opts Augroup Options and `autocmd`s for the `augroup`
local function augroup(name, opts)
  local group = vim.api.nvim_create_augroup(name, { clear = (opts.clear and true) })
  for _, cmd in ipairs(opts.autocmds) do
    cmd.opts.group = group
    vim.api.nvim_create_autocmd(cmd.event, cmd.opts)
  end
end

augroup("markdown", {
  clear = true,
  autocmds = {
    {
      event = { "BufNewFile", "BufRead" },
      opts = {
        pattern = "*.md.gotmpl",
        command = "set filetype=markdown"
      }
    },
    {
      event = { "BufNewFile", "BufRead" },
      opts = {
        pattern = { "*.md", "*.md.gotmpl" },
        callback = function()
          vim.opt_local.expandtab  = true
          vim.opt_local.tabstop    = 2
          vim.opt_local.shiftwidth = 2
          vim.opt_local.spell      = true
          vim.opt_local.textwidth  = 100
        end
      }
    },
    {
      event = { "Filetype" },
      opts = {
        pattern = "markdown",
        callback = util.enable_visual_movement
      }
    }
  },
})

augroup("fzf", {
  clear = true,
  autocmds = {
    {
      event = { "Filetype" },
      opts = {
        pattern = { "fzf" },
        callback = function()
          vim.opt_local.foldenable = false
        end
      },
    },
  },
})

augroup("saga", {
  clear = true,
  autocmds = {
    {
      event = { "Filetype" },
      opts = {
        pattern = { "sagafinder" },
        callback = function()
          vim.opt_local.foldenable = false
        end
      },
    },
  },
})


augroup("text", {
  clear = true,
  autocmds = {
    {
      event = { "Filetype" },
      opts = {
        pattern = { "text" },
        callback = function()
          vim.opt_local.spell = true
          vim.opt_local.expandtab = true
          util.enable_visual_movement()
        end
      },
    },
  },
})

augroup("yaml", {
  clear = true,
  autocmds = {
    {
      event = { "Filetype" },
      opts = {
        pattern = { "yaml" },
        callback = function()
          vim.opt_local.listchars:append { space = '∘' }
          vim.opt_local.expandtab = true
          vim.opt_local.tabstop = 2
          vim.opt_local.shiftwidth = 2
        end
      },
    },
    {
      event = { "BufRead", "BufNewFile" },
      opts = {
        pattern = { "*/templates/*.yml.j2", "*/templates/*.yaml.j2" },
        callback = function()
          vim.opt_local.filetype = "yaml.jinja2"
        end
      },
    },
  },
})
