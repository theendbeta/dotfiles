local has_nix = nil ~= string.find(string.lower(vim.loop.os_uname().version), "nixos")

local function treesitter_setup()
    require("nvim-treesitter.configs").setup({
        auto_install = true,
        -- Install languages synchronously (only applied to `ensure_installed`)
        sync_install = false,
        indent = { enable = true },
        rainbow = {
            enable = true,
            extended_mode = true,
            max_file_lines = nil,
        },
        incremental_selection = {
            enable = true,
        },
        highlight = {
            -- `false` will disable the whole extension
            enable = true,
            -- list of language that will be disabled
            disable = function(lang, buf)
                local langs_to_disable = { 'markdown', 'markdown_inline' }
                local syntax_to_disable = { 'jinja', 'just', 'toml' }
                for _, l in ipairs(langs_to_disable) do
                    if string.find(lang, l) then
                        return true
                    end
                end
                if vim.b.current_syntax == nil then
                    return false
                end
                for _, syn in ipairs(syntax_to_disable) do
                    if string.find(vim.b.current_syntax, syn) then
                        return true
                    end
                end
            end,
            -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
            -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
            -- Using this option may slow down your editor, and you may see some duplicate highlights.
            -- Instead of true it can also be a list of languages
            additional_vim_regex_highlighting = false,
        },
        ensure_installed = {
            "bash",
            "c",
            "cmake",
            "css",
            "devicetree",
            "dockerfile",
            "dot",
            "go",
            "hcl",
            "html",
            "http",
            "java",
            "javascript",
            "json",
            "json5",
            "lua",
            "make",
            "markdown",
            "markdown_inline",
            "perl",
            "python",
            "rust",
            "scss",
            "toml",
            "tsx",
            "typescript",
            "vim",
            "yaml",
        },
    })
end

return {
    'nvim-treesitter/nvim-treesitter',
    build = ":TSUpdate", -- We recommend updating the parsers on update
    config = treesitter_setup,
    cond = not has_nix,  -- nix/home-manager needs to manage tree-sitter plugin installation
}

-- vim: set expandtab ts=4 sw=4:
