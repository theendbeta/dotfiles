-- Rainbow style for yaml files
local yaml_types = {
  ["yaml"] = true,
  ["yaml.ansible"] = true,
  ["helm"] = true,
}
local function set_style()
  if yaml_types[vim.bo.filetype] then
    return {
      "#FF0000",
      "#FF7F00",
      "#FFFF00",
      "#00FF00",
      "#00FFFF",
      "#0000FF",
      "#8B00FF",
    }
  end
  return nil
end

return {
  'shellRaining/hlchunk.nvim',
  event = { "UIEnter" },
  config = function()
    local hlchunk = require('hlchunk')
    local chunk_ft = require('hlchunk.utils.filetype')
    hlchunk.setup({
      line_num = { enable = true },
      blank = { enable = true },
      chunk = {
        enable = true,
        chars = {
          horizontal_line = "─",
          vertical_line = "│",
          left_top = "┌",
          left_bottom = "└",
          right_arrow = "─",
        },
        exclude_filetypes = {
          shell = true,
          toml = true,
          helm = true,
          yaml = true,
          unpack(chunk_ft.exclude_filetypes),
        },
        style = "#00ffff",
      },
      indent = {
        enable = true,
        chars = { "│", "¦", "┆", "┊", },
        style = set_style(),
      },
    })
  end
}
