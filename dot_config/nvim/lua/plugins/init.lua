return {
  -- syntax
  { 'alker0/chezmoi.vim' },
  { 'nfnty/vim-nftables' },
  { 'NoahTheDuke/vim-just' },
  {
    'nvim-tree/nvim-web-devicons',
    opts = {
      default = true,
    }
  },
  { 'edkolev/tmuxline.vim', enabled = false },
  { 'nvim-lua/plenary.nvim' },
  {
    'preservim/tagbar',
    config = function()
      vim.keymap.set('', '<C-p>', ":TagbarToggle<CR>", { silent = true })
      -- open tagbar on right
      vim.g.tagbar_left = 0
      vim.g.tagbar_compact = 1
    end
  },
  { 'tpope/vim-dispatch' },
  { 'tpope/vim-fugitive' },
  { 'tpope/vim-vinegar',   enabled = false },
  { 'tpope/vim-commentary' },
  { 'Shougo/vimproc.vim',  build = 'make' },
  {
    'neomake/neomake',
    config = function()
      -- vim.g.neomake_verbose = 3
      vim.g.neomake_open_list = 2
      vim.g.neomake_list_height = 5
      vim.g.neomake_java_enabled_makers = { 'javac' }

      -- npm install -g csslint stylelint
      vim.g.neomake_css_enabled_makers = { 'csslint', 'stylelint' }

      vim.cmd([[
        augroup neomake_cmds
          autocmd!
          autocmd BufWrite *.vim call neomake#configure#automake('nw', 750)
          autocmd BufWrite *.pm,*.pl call neomake#configure#automake('nw', 750)
          autocmd BufWrite *.java,*.scala call neomake#configure#automake('nw', 750)
          autocmd BufWrite *.sh call neomake#configure#automake('nw', 750)
          autocmd BufReadPost,BufWritePost *.css call neomake#configure#automake('nw', 750)
        augroup END
      ]])
    end
  },
  {
    'junegunn/fzf',
    build = ":call fzf#install()",
  },
  { 'junegunn/fzf.vim' },
  {
    'vim-scripts/perl-support.vim',
    ft = { 'perl' },
    enabled = false
  },
  {
    'lervag/vimtex',
    ft = { 'tex' },
    enabled = false,
    config = function()
      -- Latex indenting
      vim.g.tex_indent_items = 1

      -- set vimtex viewer
      vim.g.vimtex_view_general_viewer = 'okular'

      --" vimtext mapping not respecting localleader
      vim.g.vimtex_mappings_enabled = 1

      vim.g.vimtex_compiler_latexmk = {
        executable = 'latexmk',
        options = {
          '-xelatex',
          '-file-line-error',
          '-synctex=1',
          '-interaction=nonstopmode',
        },
      }
    end
  },
  { 'keith/tmux.vim' },
  -- Markdown
  {
    -- Requires https://github.com/jannis-baum/Vivify
    "jannis-baum/vivify.vim",
  },
  -- Move lines (or selections)
  { 'matze/vim-move' },
  -- alignment
  {
    'junegunn/vim-easy-align',
    config = function()
      vim.keymap.set('v', '<Enter>', '<Plug>(EasyAlign)')
      vim.keymap.set('n', 'ga', '<Plug>(EasyAlign)')
    end
  },
  -- helm yaml
  { 'towolf/vim-helm' },
  -- autocompletion
  -- Basic YAML configuration
  {
    'cuducos/yaml.nvim',
    ft = { 'yaml', 'yaml.jinja2', 'yaml.ansible' },
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
    }
  },

  -- ansible
  {
    'pearofducks/ansible-vim',
    ft = { 'yaml', 'yaml.jinja2', 'yaml.ansible' },
    config = function()
      local tmpls = {}
      tmpls["*.y[a]?ml.j2"] = "yaml"
      vim.g.ansible_template_syntaxes = tmpls
    end,
    dependencies = {
      "Glench/Vim-Jinja2-Syntax",
    }
  },

  { 'hashivim/vim-terraform' },

  -- Javascript / react
  {
    'pangloss/vim-javascript',
    ft = { 'typescript', 'tsx', 'jsx', 'javascript' },
  },
  {
    'mxw/vim-jsx',
    ft = { 'typescript', 'tsx', 'jsx', 'javascript' },
    enabled = false
  },
  {
    'leafgarland/typescript-vim',
    ft = { 'typescript', 'tsx' },
  },
  {
    'peitalin/vim-jsx-typescript',
    ft = { 'typescript', 'tsx', 'jsx', 'javascript' },
    enabled = false
  },
  {
    'liuchengxu/graphviz.vim',
    enabled = false,
    config = function()
      vim.g.graphviz_output_format = 'svg'
      vim.g.graphviz_viewer = 'eog'
      vim.cmd([[
        augroup graphviz
          autocmd!
          autocmd FileType dot setlocal ts=2 sw=2 expandtab
          autocmd FileType dot nnoremap <leader>o :Graphviz!<cr>
          autocmd FileType dot nnoremap <leader>r :GraphvizCompile<cr>
          autocmd BufWritePost *.dot :GraphvizCompile
        augroup END
      ]])
    end
  },
}
