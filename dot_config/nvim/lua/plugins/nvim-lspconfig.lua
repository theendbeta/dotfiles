return {
  'neovim/nvim-lspconfig',
  config = function()
    local lspconfig = require('lspconfig')

    local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

    -- This executes from the the root nvim directory
    require('plugins.servers.ansiblels')
    require('plugins.servers.luals')
    require('plugins.servers.yamlls')
    require('plugins.servers.helmls')
    require('plugins.servers.gopls')
    require('plugins.servers.texlab')
    require('plugins.servers.nil')
    require('plugins.servers.ruff').setup(lspconfig, lsp_capabilities)
    require('plugins.servers.pyright').setup(lspconfig, lsp_capabilities)

    local lsp_defaults = { capabilities = lsp_capabilities }

    -- No additional configuration required
    lspconfig.marksman.setup(lsp_defaults)
    lspconfig.prosemd_lsp.setup(lsp_defaults)
    lspconfig.terraformls.setup(lsp_defaults)
    lspconfig.tflint.setup(lsp_defaults)
    lspconfig.golangci_lint_ls.setup(lsp_defaults)

    -- toml
    -- cargo install taplo --locked
    -- https://github.com/tamasfe/taplo
    lspconfig.taplo.setup(lsp_defaults)

    -- https://github.com/bash-lsp/bash-language-server
    -- https://github.com/mvdan/sh#shfmt
    lspconfig.bashls.setup(lsp_defaults)

    -- https://github.com/hrsh7th/vscode-langservers-extracted
    lspconfig.jsonls.setup(lsp_defaults)

    -- Global mappings.
    -- See `:help vim.diagnostic.*` for documentation on any of the below functions
    vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
    vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
    vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
    vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

    -- Use LspAttach autocommand to only map the following keys
    -- after the language server attaches to the current buffer
    vim.api.nvim_create_autocmd('LspAttach', {
      group = vim.api.nvim_create_augroup('UserLspConfig', {}),
      callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

        -- Buffer local mappings.
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        local opts = { buffer = ev.buf }
        -- vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
        -- vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
        -- vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
        -- vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
        vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
        vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
        vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
        vim.keymap.set('n', '<space>wl', function()
          print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, opts)
        -- vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
        -- vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
        -- vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, opts)
        -- vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
        vim.keymap.set('n', '<space>f', function()
          vim.lsp.buf.format { async = true }
        end, opts)



        -- autoformat on save
        local client = vim.lsp.get_client_by_id(ev.data.client_id)
        if client ~= nil and client:supports_method('textDocument/formatting') then
          vim.api.nvim_create_autocmd("BufWritePre", {
            buffer = ev.buf,
            callback = function()
              -- print(string.format('event fired: %s', vim.inspect(ev)))
              vim.lsp.buf.format({
                async = false,
                id = ev.data.client_id,
              })
            end
          })
        end
      end,
    })

    -- remove autoformat autocommand on LSP detach
    vim.api.nvim_create_autocmd('LspDetach', {
      callback = function(args)
        -- Get the detaching client
        local client = vim.lsp.get_client_by_id(args.data.client_id)
        -- Remove the autocommand to format the buffer on save, if it exists
        if client ~= nil and client:supports_method('textDocument/formatting') then
          vim.api.nvim_clear_autocmds({
            event = 'BufWritePre',
            buffer = args.buf,
          })
        end
      end,
    })
  end
}
