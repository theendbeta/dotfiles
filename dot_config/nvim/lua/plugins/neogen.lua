-- https://github.com/danymat/neogen
-- alternative: https://github.com/kkoomen/vim-doge
return {
  'danymat/neogen',
  config = function()
    local neogen = require('neogen')

    neogen.setup({
      enabled = true,
      languages = {
        python = {
          template = {
            annotation_convention = "google_docstrings"
          }
        }
      }
    })

    local opts = { noremap = true, silent = true }
    vim.api.nvim_set_keymap("n", "<Leader>nf", ":lua require('neogen').generate()<CR>", opts)
    vim.api.nvim_set_keymap("n", "<Leader>nc", ":lua require('neogen').generate({ type = 'class' })<CR>", opts)
    vim.api.nvim_set_keymap("i", "<C-l>", ":lua require('neogen').jump_next<CR>", opts)
    vim.api.nvim_set_keymap("i", "<C-h>", ":lua require('neogen').jump_prev<CR>", opts)
  end
}
