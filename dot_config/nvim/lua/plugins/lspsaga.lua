return {
  'nvimdev/lspsaga.nvim',
  config = function()
    require('lspsaga').setup({})
  end,
  event = 'LspAttach',
  dependencies = {
    'nvim-treesitter/nvim-treesitter', -- optional
    'nvim-tree/nvim-web-devicons',     -- optional
  },
  keys = {
    { '<space>D',  '<cmd>Lspsaga goto_type_definition<cr>', mode = { 'n' } },
    { '<space>pD', '<cmd>Lspsaga peek_type_definition<cr>', mode = { 'n' } },
    { 'gd',        '<cmd>Lspsaga goto_definition<cr>',      mode = { 'n' } },
    { 'gpd',       '<cmd>Lspsaga peek_definition<cr>',      mode = { 'n' } },
    { '<space>ca', '<cmd>Lspsaga code_action<cr>',          mode = { 'n' } },
    { 'K',         '<cmd>Lspsaga hover_doc<cr>',            mode = { 'n' } },
    { 'gi',        '<cmd>Lspsaga finder imp<cr>',           mode = { 'n' } },
    { '<space>rn', '<cmd>Lspsaga rename<cr>',               mode = { 'n' } },
    { 'gr',        '<cmd>Lspsaga finder def+ref<cr>',       mode = { 'n' } },
    { ',t',        '<cmd>Lspsaga term_toggle<cr>',          mode = { 'n', 't' } },
  }
}
