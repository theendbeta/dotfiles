return {
  'sainnhe/sonokai',
  lazy = false,
  priority = 1000,
  config = function()
    vim.g.sonokai_style = 'maia'
    vim.g.sonokai_enable_italic = 1
    vim.g.sonokai_disable_italic_comment = 0
    vim.g.sonokai_better_performance = 1

    vim.g.sonokai_diagnostic_text_highlight = 1
    vim.g.sonokai_diagnostic_virtual_text = 'colored'
  end
}
