require('lspconfig').yamlls.setup({
  filetypes = { "yaml", "yaml.jinja2" },
  settings = {
    yaml = {
      format = {
        enable = true,
        singleQuote = false,
        bracketSpacing = true,
      },
      schemaStore = {
        enable = true,
        url = "https://www.schemastore.org/api/json/catalog.json",
      },
      schemas = {
        kubernetes = { "*.kube.yaml", "*.kube.yml", "*.kube.yml.*" },
        -- ["https://s3.amazonaws.com/cfn-resource-specifications-us-east-1-prod/schemas/2.15.0/all-spec.json"] = {"*.cfn.yaml","*.cfn.yml"},
      },
      editor = {
        formatOnType = true,
      },
      keyOrdering = false,
      validate = true,
      hover = true,
      completion = true,
      customTags = {
        -- Ansible
        "!vault",
        -- AWS cloudformation
        "!And scalar",
        "!If scalar",
        "!If sequence",
        "!Not",
        "!Equals sequence",
        "!Or scalar",
        "!FindInMap scalar",
        "!Base64",
        "!Cidr",
        "!Ref",
        "!Sub scalar",
        "!Sub sequence",
        "!GetAtt sequence",
        "!GetAZs",
        "!ImportValue sequence",
        "!Select sequence",
        "!Split sequence",
        "!Join sequence"
      },
    }
  }
})
