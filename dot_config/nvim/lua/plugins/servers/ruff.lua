-- pipx install ruff-lsp
local ruff = {}

function ruff.setup(lspconfig, capabilities)
  lspconfig.ruff.setup({
    capabilities = capabilities or {},
    on_attach = function(client, buffnr)
      -- Disable hover in favor of pyright
      client.server_capabilities.hoverProvider = false
    end
  })
end

return ruff
