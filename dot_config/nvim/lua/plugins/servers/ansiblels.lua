require('lspconfig').ansiblels.setup({
  filetypes = { "yaml.ansible" },
  settings = {
    ansible = {
      ansible = {
        path = "LC_ALL=C.UTF-8 ansible"
      },
      validation = {
        enabled = true,
        lint = {
          enabled = true,
          arguments = "--format pep8",
          path = "LC_ALL=C.UTF-8 ansible-lint",
        }
      }
    }
  }
})
