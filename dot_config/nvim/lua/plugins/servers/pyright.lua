-- https://microsoft.github.io/pyright/#/installation
local pyright = {}

function pyright.setup(lspconfig, capabilities)

  lspconfig.pyright.setup({
    capabilities = capabilities or {},
    settings = {
      pyright = {
        disableOrganizeImports = true, -- using ruff/ruff_lsp
        disableTaggedHints = true,
      },
      python = {
        analysis = {
          typeCheckingMode = "standard",
          diagnosticSeverityOverrides = {
            -- https://github.com/microsoft/pyright/blob/main/docs/configuration.md#type-check-diagnostics-settings
            reportUndefinedVariable = "none",
          },
          -- ignore = { '*' }, -- using ruff/ruff_lsp
          -- typeCheckingMode = 'off', -- using mypy?
        },
      },
    },
  })

end

return pyright
