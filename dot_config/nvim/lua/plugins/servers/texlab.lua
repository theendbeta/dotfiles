require('lspconfig').texlab.setup({
  settings = {
    build = {
      args = { "-pdf", "-interaction=nonstopmode", "-synctex=1", "%f" },
      executable = "latexmk",
      forwardSearchAfter = true,
      onSave = true
    },
    chktex = {
      onEdit = true,
      onOpenAndSave = true
    },
    diagnosticsDelay = 300,
    formatterLineLength = 80,
    forwardSearch = {
      executable = "okular",
      args = {"-f", "%l", "%p", "\"code -g %f:%l\""},
    },
    latexFormatter = "latexindent",
    latexindent = {
      modifyLineBreaks = false
    }
  }
})
