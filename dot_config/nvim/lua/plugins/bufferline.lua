local function setup()
  require("bufferline").setup {
    options = {
      separator_style = "slant",
      numbers = "both",
      hover = {
        enabled = true,
        delay = 200,
        reveal = { 'close' }
      }
    }
  }

  local options = { noremap = true, silent = true }

  vim.api.nvim_set_keymap('n', ']b', ':BufferLineCycleNext<CR>', options)
  vim.api.nvim_set_keymap('n', '[b', ':BufferLineCyclePrev<CR>', options)

  -- These commands will move the current buffer backwards or forwards in the bufferline
  vim.api.nvim_set_keymap('n', '<leader>b+', ':BufferLineMoveNext<CR>', options)
  vim.api.nvim_set_keymap('n', '<leader>b-', ':BufferLineMovePrev<CR>', options)

  -- These commands will sort buffers by directory, language, or a custom criteria
  vim.api.nvim_set_keymap('n', '<leader>be', ':BufferLineSortByExtension<CR>', options)
  vim.api.nvim_set_keymap('n', '<leader>bd', ':BufferLineSortByDirectory<CR>', options)
  -- nnoremap <silent><mymap> :lua require'bufferline'.sort_buffers_by(function (buf_a, buf_b) return buf_a.id < buf_b.id end)<CR>

  for i = 1, 9, 1 do
    vim.api.nvim_set_keymap('n', '<leader>' .. i, '<Cmd>BufferLineGoToBuffer' .. i .. '<CR>', options)
  end
end

return {
  'akinsho/bufferline.nvim',
  dependencies = { 'nvim-tree/nvim-web-devicons', opt = true },
  version = '*',
  config = setup
}
