local function setup()
  local rt = require("rust-tools")
  rt.setup({
    tools = {
      inlay_hints = {
        -- From `:highlight`
        highlight     = 'LspDiagnosticsVirtualTextHint',
        max_len_align = true,
      }
    },
    server = {
      cmd = { "rustup", "run", "stable", "rust-analyzer" },
      on_attach = function(_, bufnr)
        -- Hover actions
        vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
        -- Code action groups
        vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
      end,
    },
  })

  rt.inlay_hints.enable()

  vim.api.nvim_create_autocmd('BufWritePre', {
    pattern = '*.rs',
    callback = function()
      vim.lsp.buf.format({ async = false })
    end
  })
end

return {
  'simrat39/rust-tools.nvim',
  config = setup,
}
