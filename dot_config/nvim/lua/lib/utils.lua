local M = {}

M.visual_movement_keymap = {
  ['^'] = 'g^',
  ['$'] = 'g$',
  ['k'] = 'gk',
  ['j'] = 'gj',
}

M.enable_visual_movement = function()
  for k, v in pairs(M.visual_movement_keymap) do
    vim.keymap.set('n', k, v, { buffer = 0 })
  end
end

M.toggle_visual_movement = function()
  local keymap = vim.api.nvim_buf_get_keymap(0, 'n')

  for k, v in pairs(M.visual_movement_keymap) do
    if keymap[k] == v then
      for k2, _ in pairs(M.visual_movement_keymap) do
        vim.keymap.del('n', k2, { buffer = 0 })
      end
      return
    end
    -- If we find an existing one, they will all be deleted, so we are safe to set here
    vim.keymap.set('n', k, v, { buffer = 0 })
  end
end

return M
