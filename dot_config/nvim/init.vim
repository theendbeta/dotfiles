set encoding=utf-8
scriptencoding utf-8
set fileformats=unix
let g:encoding_set = 1

filetype plugin indent on

" if has('mac')
" 	let g:python3_host_prog='/Library/Frameworks/Python.framework/Versions/3.7/bin/python3'
" else
" 	let g:python3_host_prog='/usr/bin/python3'
" endif

" Always show statusline
set laststatus=2
set t_Co=256

"""""""""""""""""""""""""""""""""""""
" Syntax Highlighting
" Colorschemes
"""""""""""""""""""""""""""""""""""""
syntax enable

if has('termguicolors')
  set termguicolors
endif

set background=dark

" Set comments to use italics
highlight Comment cterm=italic

" change the mapleader from '\'
let mapleader=','
let maplocalleader=';'

set autoindent "ai
set smartindent "si

" mouse mode that works well with tmux
set mouse=a

nnoremap <A-a> <C-A>

" disable ctrl-a
map <C-A> <nop>

" show line numbers
set number
set relativenumber

" Fold using treesitter
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

" function FoldConfig()
" 	set foldmethod=expr
" 	set foldexpr=nvim_treesitter#foldexpr()
" endfunction

" autocmd BufAdd,BufEnter,BufNew,BufNewFile,BufWinEnter * :call FoldConfig()


" powerful backspaces
set backspace=indent,eol,start

" highlight the searchterms
set hlsearch

" jump to the matches while typing
set incsearch

" ignore case while searching
set ignorecase

" don't wrap words
set textwidth=120

" history
set history=50

" 1000 undo levels
set undolevels=1000

" show a ruler
set ruler

" show partial commands
set showcmd

" show matching braces
set showmatch

" Wrap too long lines
set wrap

" spaces, not tabs
set expandtab

" Tabs are 4 characters
set tabstop=2

" (Auto)indent uses 4 characters
set shiftwidth=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Terminal Options
tnoremap <C-w>h <C-\><C-n><C-w>h
tnoremap <C-w>j <C-\><C-n><C-w>j
tnoremap <C-w>k <C-\><C-n><C-w>k
tnoremap <C-w>l <C-\><C-n><C-w>l
tnoremap <Esc> <C-\><C-n>


" enter insert mode on return to terminal
augroup terminal
	autocmd!
	autocmd BufWinEnter,WinEnter term://* startinsert
	autocmd TermOpen * setlocal nonumber norelativenumber
	autocmd TermOpen * startinsert
	autocmd BufLeave term://* stopinsert
augroup END

" 2 character tabs for .tex files
augroup latex
	autocmd!
	autocmd BufNewFile,BufRead *.tex set expandtab tabstop=2 shiftwidth=2
	autocmd BufNewFile,BufRead *.tex set makeprg=latexmk\ -f\ -pdf\ -interaction=nonstopmode "%"
augroup END

augroup html
	autocmd!
	autocmd FileType html set expandtab tabstop=2 shiftwidth=2
augroup END

augroup javascript
	autocmd!
	autocmd FileType javascript,javascript.jsx set tabstop=4 shiftwidth=4
augroup END

augroup json
	autocmd!
	autocmd FileType json set expandtab tabstop=2 shiftwidth=2
augroup END

augroup lua
	autocmd!
	autocmd FileType lua set expandtab tabstop=2 shiftwidth=2
augroup END

augroup golang
	autocmd!
  autocmd FileType go set tabstop=4 shiftwidth=4
	autocmd BufNewFile,BufRead *.html.gotmpl set filetype=gohtmltmpl
augroup END


"autocmd BufWrite *.py call UpdateTags()

" Show indentation
" set listchars=tab:>.,trail:~,extends:>,precedes:<,nbsp:.
" tab: U+25B8, U+00B7 / space: U+2218 / trail: U+203A
" nbsp: U+23B5 / precedes: U+21A2 / extends: U+21A3
set listchars=tab:▸·,trail:›,nbsp:⎵,precedes:↢,extends:↣
" set listchars+=space:∘
set list

" Expand the command line using tab
set wildchar=<Tab>

" Enable copy/cut/paste using system clipboard
set clipboard+=unnamedplus

" enable italics
set t_ZH=<Esc>[3m
set t_ZR=<Esc>[23m

""""""""""""""""""""""""""""""""""""
" Java
""""""""""""""""""""""""""""""""""""

augroup java
	autocmd!
	autocmd FileType java setlocal makeprg=javac\ %
augroup end

""""""""""""""""""""""""""""""""""""
" Python
""""""""""""""""""""""""""""""""""""
augroup python
	autocmd!
	autocmd FileType python set expandtab
augroup END


""""""""""""""""""""""""""""""""""
" Functions
""""""""""""""""""""""""""""""""""
function FormatJSON()
	:%! python -m json.tool
endfunction
command PrettyJSON :call FormatJSON()

""""""""""""""""""""""""""""""
" XML
""""""""""""""""""""""""""""""
command PrettyXML :silent %!xmllint --format --recover - 2>/dev/null


let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"
let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"

""""""""""""""""""""""""""""""""
" lua requires
""""""""""""""""""""""""""""""""
lua require('init')
