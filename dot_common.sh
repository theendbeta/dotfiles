#!/bin/bash

# shellcheck source=./private_dot_common/utils.sh
source "$HOME/.common/utils.sh"

#####################################################################
#
# EXPORTS
#
#####################################################################
export EDITOR=nvim

# beets
export BEETSDIR="$HOME/.config/beets/"

# ansible
export ANSIBLE_VAULT_PASSWORD_FILE="$HOME/.vaultpw"

export RIPGREP_CONFIG_PATH="$HOME/.config/ripgreprc"

######################################################################
#
# FZF
#
######################################################################
# export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_DEFAULT_COMMAND='rg --files --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

######################################################################
#
# PATH additions
#
######################################################################

_add_to_path "$HOME/.local/bin"
_add_to_path "$HOME/.cargo/bin"
_add_to_path "/usr/local/go/bin"
_add_to_path "$HOME/go/bin"
_add_to_path "/snap/bin" 
_add_to_path "$HOME/.yarn/bin"
_add_to_path "$HOME/Applications/Bento4/bin"

# pyenv
if command -v pyenv 1>/dev/null 2>&1; then
	eval "$(pyenv init -)"
fi


if [ -f "$HOME/.aliases" ]; then
  source "$HOME/.aliases"
fi


# Source all files in $HOME/.common, if the directory exists
if [ -d "$HOME/.common" ] ; then
	for f in "$HOME"/.common/*; do
		if [ -f "$f" ]; then
			source "$f";
		fi
	done
fi
